const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var FirmaSchema = new Schema({
    FirmaAdi                : {type: String, required: false, default: null},
    FirmaUnvani             : {type: String, required: false, default: null},
    FirmaYetkilisiAdi       : {type: String, required: false, default: null},
    FirmaYetkilisiSoyadi    : {type: String, required: false, default: null},
    FirmaYetkilisiCepNo     : {type: String, required: false, default: null},
    FirmaYetkilisiSabitNo   : {type: String, required: false, default: null},
    FirmaAdres              : {type: String, required: false, default: null},
    FirmaIl                 : {type: String, required: false, default: null},
    FirmaIlce               : {type: String, required: false, default: null},
    YetkiliSMMMAdi          : {type: String, required: false, default: null},
    YetkiliSMMMSoyadi       : {type: String, required: false, default: null},
    YetkiliSMMMCepNo        : {type: String, required: false, default: null},
    YetkiliSMMMSabitNo      : {type: String, required: false, default: null},
    IslemYetkilisiAdi       : {type: String, required: false, default: null},
    IslemYetkilisiSoyadi    : {type: String, required: false, default: null},
    IslemYetkilisiCepNo     : {type: String, required: false, default: null},
    IslemYetkilisiSabitNo   : {type: String, required: false, default: null},
    SMMMAdres               : {type: String, required: false, default: null},
    SMMMIl                  : {type: String, required: false, default: null},
    SMMMIlce                : {type: String, required: false, default: null},
    VergiDariresi           : {type: String, required: false, default: null},
    VergiNo                 : {type: String, required: false, default: null},
    SistemKullaniciAdi      : {type: String, required: false, default: null},
    SistemSfiresi1          : {type: String, required: false, default: null},
    SistemSifresi2          : {type: String, required: false, default: null},
    SGKCari                 : {type: Boolean, required: false, default: null},
    SGKGecmis               : {type: Boolean, required: false, default: null},
    Note                    : {type: String, required: false, default: null},
    UserId                  : {type: mongoose.Schema.Types.ObjectId, required: false, default: null, ref:'User'},
    UpdateDate              : {type: Date, required: false, default: null},
    RandevuTarihi           : {type: Date, required: false, default: null},
    Status                  : {type: Number, required: false, default: null},
    KullaniciKodu           : {type: String, required: false, default: null},
    IslemYetkilisiMail      : {type: String, required: false, default: null},
    YetkiliSMMMMail         : {type: String, required: false, default: null},
    SirketSahipleriTel      : {type: String, required: false, default: null},
    FirmaMail               : {type: String, required: false, default: null},
    Sifreler                : {type: Array},
    SistemeYuklendi         : {type: Boolean, required: false, default: false}
    },
    {
        versionKey: false
    }
)

var UserSchema = new Schema({
    UserName  : {type: String, required: false, default: null},
    Password  : {type: String, required: false, default: null},
    FirstName : {type: String, required: false, default: null},
    LastName  : {type: String, required: false, default: null},
    isAdmin   : {type: Boolean, required: false, default: false}
},
    {
        versionKey: false
    }
)

var Firma    = mongoose.model('Firmas', FirmaSchema);
var User     = mongoose.model('Users',  UserSchema);

module.exports = {
    User:User,
    Firma:Firma
}