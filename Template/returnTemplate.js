
function returnJson(data, message,status =200) {
    return {
        status:status,
        data: data,
        messages: message
    }

}

module.exports = returnJson;
