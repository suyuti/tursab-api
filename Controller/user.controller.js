var     express         = require('express');
var     router          = express.Router();
const   userService     = require('../Services/user.service');
const   returnTemplate    = require('../Template/returnTemplate');
const   httpResult        = require('../config');

router.post     ('/authenticate',   auth);
router.get      ('/',       getAll);
router.get      ('/:id',    getById);
router.post     ('/',       create);
router.put      ('/:id',    update);
router.delete   ('/:id',    remove);


function auth(req, res, next) {
    userService.authenticate(req.body).then(data => {
        console.log(data)
        if (data) {
            res.json(returnTemplate(data, ""))
        } else {
            res.json(returnTemplate([], "Username or password is incorrect",httpResult.error))
        }
    })
    .catch(err => next(err));
}
function getAll(req, res, next) {
    userService.getAll().then(data => {
        if (data.length) {
            res.json(returnTemplate(data, ""))
        } else {
            res.json(returnTemplate([], "Kayıt bulunamamıştır", httpResult.error))
        }
    })
    .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id).then(data => {
        if (data) {
            res.json(returnTemplate(data, ""))
        } else {
            res.json(returnTemplate([], "Kayıt bulunamamıştır", httpResult.error))
        }
    })
    .catch(err => next(err));
}

function create(req, res, next) {
    userService.create(req.body).then(data => {
        if (data.length) {
            res.json(returnTemplate(data, ""))
        } else {
            res.json(returnTemplate([], "Kayıt bulunamamıştır", httpResult.error))
        }
    })
    .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body).then(data => {
        if (data) {
            res.json(returnTemplate(data, ""))
        } else {
            res.json(returnTemplate([], "Kayıt bulunamamıştır", httpResult.error))
        }
    })
    .catch(err => next(err));
}

function remove(req, res, next) {
    userService.remove(req.params.id).then(data => {
        if (data) {
            res.json(returnTemplate(data, ""))
        } else {
            res.json(returnTemplate([], "Kayıt bulunamamıştır", httpResult.error))
        }
    })
    .catch(err => next(err));
}


module.exports = router;
