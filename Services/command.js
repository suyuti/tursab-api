const config = require('../config.json');
const jwt = require('jsonwebtoken');
const userService = require("../Services/user.service")
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const dotenv = require('dotenv').config()
const {User} = require('../Models/tursab.model');

var DB_USER     = process.env.DB_USER
var DB_PASSWORD = process.env.DB_PASSWORD
var DB_DATABASE = process.env.DB_DATABASE
var DB_HOST     = process.env.DB_HOST
var DB_PORT     = process.env.DB_PORT


const dbURI = `mongodb://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}`;
//const dbURI = "mongodb://67.205.130.187:27017/tursab";
const options = {
    reconnectTries: Number.MAX_VALUE,
    poolSize: 10,
    useNewUrlParser : true,
    useUnifiedTopology: true,
    useFindAndModify: false
};

function databaseConnection() {
    mongoose.connect(dbURI, options).then(
        () => {
            console.log('DB baglandi')
            //User.find({}, (e,d) => {
            //    console.log(d)
            //})
        },
        err => {
            console.log('DB baglandi hatasi ', err)
        }
    )
}

let publicUrl = [
    "/users/authenticate"
]


function TokenKontrol(req, res, next) {
    //public url kontrolü
    for (let index = 0; index < publicUrl.length; index++) {
        if (publicUrl[index] === req.url) {
            next();
            return;
        }
    }

    let token = req.headers['x-access-token'] || req.headers['authorization'];
    if (!token) {
        return res.json({
            success: false,
            message: 'Token YOK Lütfen Giriş Yapınız'
        });
    }

    if (token) {
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Token Geçerli Degil Lütfen Tekrar Giriş Yapınız.'
                });
            } else {
                userService.getById(decoded._id)
                    .then(user => {
                        if (user) {
                            req.TokenUser = user;
                            req.decoded = decoded;
                            next();
                        }
                        else {
                            res.json({ success: false, message: 'Token Geçerli Kullanıcı Bulununamadı Lütfen bilgilerinizi Kontrol Ediniz.' })
                        }
                    })
                    .catch(err => next(err));
            }
        });
    } 
    else {
        return res.json({
            success: false,
            message: 'Token YOK Lütfen Giriş Yapınız'
        });
    }
}

module.exports = {
    TokenKontrol,
    databaseConnection,
}