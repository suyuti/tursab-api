const config = require('../config.json');
const jwt = require('jsonwebtoken');
const {Firma} = require('../Models/tursab.model');
const states = require('./states');
const moment = require('moment');

async function getNext(currentUser) {
    var fourHourAgo = moment().subtract(4, 'hours');
    var simdi = moment()

    var f = await Firma.find({Status:states.INPROGRESS, UserId:currentUser, RandevuTarihi: {$lt: simdi}}).exec();
    if (f.length > 0) {
        console.log('Atanmis firma var ' + f[0].FirmaAdi)
        return f;
    }


    var aranabilecekler = await Firma.find({Status: {$in: [ states.ULASILAMADI, 
                                                            states.MUSAIT_DEGIL]},
                                            UpdateDate: {$lt: fourHourAgo}}).exec();
    if (aranabilecekler.length > 0) {
        aranabilecekler[0].Status = states.INPROGRESS;
        aranabilecekler[0].UserId = currentUser
        aranabilecekler[0].UpdateDate = new Date()
        //console.log(aranabilecekler[0])
        let r = await Firma.findByIdAndUpdate(aranabilecekler[0]._id, aranabilecekler[0], {new:true})
        return [r];
    }

//    console.log(`${simdi}`)
    var randevulular = await Firma.find({Status: {$in: [states.TEKRAR_ARANACAK, 
                                                        states.SIFRE_BEKLENIYOR]},
                                         RandevuTarihi: {$lt: simdi}}).exec();
    if (randevulular.length > 0) {
        randevulular[0].Status = states.INPROGRESS;
        randevulular[0].UserId = currentUser
        randevulular[0].UpdateDate = new Date()
//        console.log(`${simdi} randevulu: ${randevulular[0]}`)
        let r = await Firma.findByIdAndUpdate(randevulular[0]._id, randevulular[0], {new:true})
        return [r];
    }
    else {
        var n = await Firma.find({Status: {$in: [states.ACIK, 
                                                //states.ULASILAMADI, 
                                                //states.TEKRAR_ARANACAK, 
                                                //states.MUSAIT_DEGIL, 
                                                //states.SIFRE_BEKLENIYOR
                                            ]}}).exec();
        if (n.length > 0) {
        n[0].Status = states.INPROGRESS;
        n[0].UserId = currentUser
        n[0].UpdateDate = new Date()
        let r = await Firma.findByIdAndUpdate(n[0]._id, n[0], {new:true})
        //console.log(r)
        return [r];
        }
    }
    
}

async function getAll() {
    let res = await Firma.find({});
    return res;
}

async function getById(id) {
    let res = await Firma.findById(id);
    return res;
}

async function create(firma) {
    let res = await Firma.create(firma);
    return res;
}

async function update(id, firma) {
    //delete firma.FirmaAdi;
    //console.log(firma)
    firma.UpdateDate = new Date()
    let res = await Firma.updateOne({_id:id}, firma);
    return res;
}

async function remove(id) {
    let res = await Firma.findByIdAndDelete(id);
    return res;
}


module.exports = {
    getAll,
    getById,
    create,
    update,
    remove,
    getNext
};

/*
{
    "FirmaAdi"                : "Firma1",
    "FirmaUnvani"             : "",
    "FirmaYetkilisiAdi"       : "",
    "FirmaYetkilisiSoyadi"    : "",
    "FirmaYetkilisiCepNo"     : "",
    "FirmaYetkilisiSabitNo"   : "",
    "FirmaAdres"              : "",
    "FirmaIl"                 : "",
    "FirmaIlce"               : "",
    "YetkiliSMMMAdi"          : "",
    "YetkiliSMMMSoyadi"       : "",
    "YetkiliSMMMCepNo"        : "",
    "YetkiliSMMMSabitNo"      : "",
    "IslemYetkilisiAdi"       : "",
    "IslemYetkilisiSoyadi"    : "",
    "IslemYetkilisiCepNo"     : "",
    "IslemYetkilisiSabitNo"   : "",
    "SMMMAdres"               : "",
    "SMMMIl"                  : "",
    "SMMMIlce"                : "",
    "VergiDariresi"           : "",
    "VergiNo"                 : "",
    "SistemKullaniciAdi"      : "",
    "SistemSfiresi1"          : "",
    "SistemSifresi2"          : "",
    "SGKCari"                 : "",
    "SGKGecmis"               : "",
    "Note"                    : "",
    "Status"                  : 0
}
*/