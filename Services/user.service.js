const config = require('../config.json');
const jwt = require('jsonwebtoken');
const {User} = require('../Models/tursab.model');

async function authenticate({ userName, userPassword }) {
    //console.log(userName, userPassword)
    const res = await User.find({UserName:userName, Password: userPassword});
    //console.log(res)
    if (res.length > 0) {
        var user = res[0]
        const token = jwt.sign({ _id: user._id, role: user.Password }, config.secret);
        return {
            UserName    :   user.UserName, 
            FirstName   :   user.FirstName, 
            LastName    :   user.LastName, 
            token       :   token, 
            IsAdmin     :   user.isAdmin,
            _id         :   user._id
        }
    }
}

async function getById(id) {
    let res = await User.findById(id);
    return res;
}

async function getBy_Username_Password(username, password) {
    User.find({"UserName":username, "Password": password}, (err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            return data[0];
        }
    })
}

module.exports = {
    authenticate,
    getById,
};
